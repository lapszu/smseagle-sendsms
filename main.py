import argparse

SMSEAGLE_USERNAME = "smssender"
SMSEAGLE_PASSWORD = "topsecretpassword"
SMSEAGLE_GATEWAY = "https://sms.example.com"


def create_ssl_context(ssl_verification):
    """Function creates default context with ssl.CERT_NONE, to disable SSL certificate verification."""
    import ssl
    context = ssl.create_default_context()
    if ssl_verification is False:
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
    return context


def sendsms_get(smsgw_hostname, smsgw_username, smsgw_password, smsgw_recipient, smsgw_message, ssl_verification):
    """Function connects to SMSEagle gateway with http method GET."""
    from urllib.request import urlopen
    from urllib.parse import urlencode
    base_url = smsgw_hostname + '/index.php/http_api/send_sms'
    query_args = {
        'login': smsgw_username,
        'pass': smsgw_password,
        'to': smsgw_recipient,
        'message': smsgw_message
    }
    encoded_args = urlencode(query_args)
    url = base_url + '?' + encoded_args
    ssl_context = create_ssl_context(ssl_verification)
    result = urlopen(url, context=ssl_context).read()
    return result


def sendsms_post(smsgw_hostname, smsgw_username, smsgw_password, smsgw_recipient, smsgw_message, ssl_verification):
    """Function connects to SMSEagle gateway with http method POST."""
    from urllib.request import urlopen, Request
    import json
    base_url = smsgw_hostname + '/index.php/jsonrpc/sms'
    query_args = {
        'method': 'sms.send_sms',
        'params': {
            'login': smsgw_username,
            'pass': smsgw_password,
            'to': smsgw_recipient,
            'message': smsgw_message
        }
    }
    json_data = json.dumps(query_args).encode('UTF-8')
    req = Request(base_url)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    req.add_header('Content-Length', str(len(json_data)))
    ssl_context = create_ssl_context(ssl_verification)
    result = urlopen(req, data=json_data, context=ssl_context).read()
    return result


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Specify recipient mobile number and message content")
    parser.add_argument('--sms-sendto', nargs=1, required=True,
                        help="Mobile number of recipient in format +48XXXXXXXXX.")
    parser.add_argument('--sms-message', nargs=1, required=True, help="Message content to send.")
    parser.add_argument('--sms-gateway', nargs=1, default=[SMSEAGLE_GATEWAY],
                        help="Hostname or IP of SMSEagle gateway.")
    parser.add_argument('--username', nargs=1, default=[SMSEAGLE_USERNAME],
                        help="Username to authenticate against SMSEagle gateway.")
    parser.add_argument('--password', nargs=1, default=[SMSEAGLE_PASSWORD],
                        help="Password to authenticate against SMSeagle gateway.")
    parser.add_argument('--http-method', nargs=1, default=['POST'],
                        help="Select a http method to use to connect to SMSEagle gateway", choices=['GET', 'POST'])
    parser.add_argument('--disable-ssl-verification', nargs='?', default='', type=bool,
                        help="Disable certificate verification, useful when self-signed certificate is used.")
    args = parser.parse_args()

    sms_sendto = args.sms_sendto[0]
    sms_message = args.sms_message[0]
    sms_gateway = args.sms_gateway[0]
    username = args.username[0]
    password = args.password[0]
    http_method = args.http_method[0]
    # This is a tricky part of argument parsing, because type=bool converts empty string to False, and
    # not empty string to True. We have twisted logic here.
    if args.disable_ssl_verification is not False and args.disable_ssl_verification != 'None':
        ssl_verify = False
    else:
        ssl_verify = True

    print(f"I'm going to send message: \"{sms_message}\" to {sms_sendto}, as user {username}, via {sms_gateway}, "
          f"using method {http_method}.")
    if ssl_verify is False:
        print(f"Certificate of SMSEagle gateway will not by verified.")
    # print(f"SMSEagle username: {username}, SMSEagle password: {password}.")

    if http_method == 'POST':
        out = sendsms_post(sms_gateway, username, password, sms_sendto, sms_message, ssl_verify)
    else:
        out = sendsms_get(sms_gateway, username, password, sms_sendto, sms_message, ssl_verify)

    print(f"Message has been sent successfully: {out}")
